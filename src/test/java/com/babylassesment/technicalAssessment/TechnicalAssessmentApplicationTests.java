package com.babylassesment.technicalAssessment;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.Model;
import com.babylassesment.technicalAssessment.RomanCalc.RomanNumCalculatorController;
import com.babylassesment.technicalAssessment.utills.HomeController;

@RunWith(SpringRunner.class)
@SpringBootTest
@SuppressWarnings("unlikely-arg-type")
public class TechnicalAssessmentApplicationTests {
	
	@MockBean
	private Model model;
	
	@Test
	public void contextLoads() {
	}
	
	@Test
    public void TestHomeController() {
		HomeController homeController = new HomeController();
        String result = homeController.landindPage();
        assertEquals(result, "index");
    }
	
	@Test
    public void TestRomanNumCalculator() {
		RomanNumCalculatorController roman = new RomanNumCalculatorController();
		String leftOperand = "X";
		String operator ="X";
		String rightOperand ="+";
		
		equals(roman.index(leftOperand, operator, rightOperand, model));
		
    }
	
	@Test
    public void TestRomanNumConverter() {
		RomanNumCalculatorController roman = new RomanNumCalculatorController();
		String a = "10";
		
		equals(roman.convert(model, a));
		
    }

}
