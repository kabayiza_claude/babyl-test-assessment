package com.babylassesment.technicalAssessment.fizzBuzz;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class FizzBuzzController {

	@GetMapping("/result.do")
	public static void fizzBuzz(Model model){
		
		try {

			List<String> listString = new ArrayList<String>();

			List<String> flamingo = new ArrayList<String>();

			List<String> pinkFlamingo = new ArrayList<String>();

			List<Integer> fibonacci = new ArrayList<Integer>();
			
			int f0 = 0, f1 = 1;
	
			for(int i= 0; i <= 100; i++){
				String output = "";
				String outputFlamingo ="";
				String outputPinkFlamingo ="";
				if(i % 3 == 0) output += "Fizz";
				if(i % 5 == 0) output += "Buzz";
				if(output.equals("")) output += i;
				
				listString.add(output);

				 fibonacci.add(f0);
				 if(fibonacci.contains(i)) outputFlamingo ="Flamingo";
	
				 if(fibonacci.contains(i) && f0 % 3 == 0 && f0 % 5 == 0) outputPinkFlamingo ="Pink Flamingo";
				 
				 int sum = f0 + f1;
		          f0 = f1;
		          f1 = sum;
		          flamingo.add(outputFlamingo);
		          
		          pinkFlamingo.add(outputPinkFlamingo);
		          
		          
			}

			model.addAttribute("output", listString);
			model.addAttribute("outputFlamingo", flamingo);
			model.addAttribute("outputPinkFlamingo", pinkFlamingo);
		} catch (Exception e) {
			// TODO: handle exception
		}	
	}
	
}
