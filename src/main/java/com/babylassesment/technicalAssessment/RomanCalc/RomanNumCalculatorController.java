package com.babylassesment.technicalAssessment.RomanCalc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.babylassesment.technicalAssessment.model.Calculator;
import com.babylassesment.technicalAssessment.utills.Converter;

@Controller
public class RomanNumCalculatorController {
	
	Converter converter = new Converter();
	
	@GetMapping("/numRoman.do")
	public String index(Model model) {
		model.addAttribute("operator", "+");
		model.addAttribute("view", "numRoman");
		return "base-layout";
	}
	
	@PostMapping("/numRoman.do")
	public String index(
			@RequestParam String leftOperand,
			@RequestParam String operator,
			@RequestParam String rightOperand,
			Model model
	) {
		double leftNumber;
		double rightNumber;

		
		try {
			//convert entered roman to number
			leftNumber = converter.toNumerical(leftOperand.toUpperCase());

		}
		catch (NumberFormatException ex) {
			leftNumber = 0;
		}

		try {
			
			rightNumber = converter.toNumerical(rightOperand.toUpperCase());
		}
		catch (NumberFormatException ex) {
			rightNumber = 0;
		}
		
		Calculator calculator = new Calculator(
				leftNumber,
				rightNumber,
				operator
		);
		
		double result = calculator.calculateResult();
		
		model.addAttribute("leftOperand", converter.toRoman((int) leftNumber) );
		model.addAttribute("operator", operator);
		model.addAttribute("rightOperand", converter.toRoman((int) rightNumber));
		model.addAttribute("result", converter.toRoman((int) result));

		model.addAttribute("view", "numRoman");
		return "base-layout";
	}
	
	@RequestMapping("/convert.do")
	public String convertingPage() {

		return "Convert";
	}
	
	@RequestMapping(value= "/convert.do" , method=RequestMethod.POST)
	public String convert(Model model, String number) {

		try 
			{
			if (number.isEmpty()) throw new RuntimeException("Emty From");
			
			try 
				{
				
				Integer Numconverted=Integer.parseInt(number);
		  if(Numconverted>0) {
					 String Roman_num=converter.toRoman(Numconverted);
						 model.addAttribute("result", Roman_num);
								}
					else  {model.addAttribute("out", "Out of range");}
	
				}
				catch (Exception Arithmetic_exception) 
					 {
				
						 try {
					 int convnum;
						 convnum = converter.toNumerical(number.toUpperCase());
						 model.addAttribute("result", convnum);
					 }
						 catch (Exception excceptionmodel1)
						  {
						 model.addAttribute("expmodel", excceptionmodel1);
							 }
					 	}
			} 
			catch (Exception excceptionmodel2) 
			{
			
			model.addAttribute("expmodel", excceptionmodel2);
			}
		
		return "Convert";
	}
	
	@RequestMapping("/rest.do")
	public String restPage() {

		return "rest";
	}
	
}