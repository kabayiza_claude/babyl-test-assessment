package com.babylassesment.technicalAssessment.utills;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
	
	@GetMapping(value = { "/", "/home.do" })
	public String landindPage() {
		return "index";
	}
}
