package com.babylassesment.technicalAssessment.utills;

public class Converter {
	
	StaticValue staticVal = new StaticValue();
	    /**
	     * Method used to convert a string to a integer
	     */
	    public int toNumerical(String roman) {
	        for (int i = 0; i < staticVal.NUMBER_LETTERS.length; i++) { 
	            if(roman.startsWith(staticVal.NUMBER_LETTERS[i]))
	                return staticVal.NUMBER_VALUES[i] + toNumerical(roman.replaceFirst(staticVal.NUMBER_LETTERS[i], ""));
	        }
	        return 0; 
	    }
	    /**
	     * Method used to convert a integer to a roman numeral
	     */
	    public String toRoman(int num) {
	        String roman = ""; 
	        for (int i = 0; i < staticVal.NUMBER_VALUES.length; i++) { 
	            while (num >= staticVal.NUMBER_VALUES[i]) {
	                roman += staticVal.NUMBER_LETTERS[i]; 
	                num -= staticVal.NUMBER_VALUES[i]; 
	            }
	        }
	        return roman;
	    }
	    /**
	     * Method used to check if a string is an integer
	     */
	    public boolean isNumber(String s) {
	        try {
	            Integer.parseInt(s);
	        }catch(NumberFormatException e) {
	            return false;
	        }
	        return true;
	    }
}
