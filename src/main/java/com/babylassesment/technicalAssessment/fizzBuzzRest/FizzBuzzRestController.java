package com.babylassesment.technicalAssessment.fizzBuzzRest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FizzBuzzRestController {
	
	@RequestMapping(value="/api/fizzBuzzRest.do", method=RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> occasionalListRequest(HttpServletRequest request) throws Exception{
		 Map<String, Object> map = new HashMap<String, Object>();
			try {

				List<String> listString = new ArrayList<String>();

				List<String> flamingo = new ArrayList<String>();

				List<String> pinkFlamingo = new ArrayList<String>();

				List<Integer> fibonacci = new ArrayList<Integer>();

				int f0 = 0, f1 = 1;

				for(int i= 0; i <= 100; i++){
					String output = "";
					String outputFlamingo ="";
					String outputPinkFlamingo ="";
					if(i % 3 == 0) output += "Fizz";
					if(i % 5 == 0) output += "Buzz";
					if(output.equals("")) output += i;
					
					listString.add(output);
					 fibonacci.add(f0);
					 if(fibonacci.contains(i)) outputFlamingo ="Flamingo";
					 if(fibonacci.contains(i) && f0 % 3 == 0 && f0 % 5 == 0) outputPinkFlamingo ="Pink Flamingo";
					 
					 int sum = f0 + f1;
			          f0 = f1;
			          f1 = sum;
			          flamingo.add(outputFlamingo);
			          
			          pinkFlamingo.add(outputPinkFlamingo);
		     
				}
				map.put("output", listString);
				map.put("outputFlamingo", flamingo);
				map.put("outputPinkFlamingo", pinkFlamingo);
				
				
				return new ResponseEntity<Map<String, Object>> (map, HttpStatus.OK);
			} catch (Exception e) {
				 map.put("message", e);
				 return new ResponseEntity<Map<String, Object>> (map, HttpStatus.BAD_REQUEST);
			}	
	}

}
